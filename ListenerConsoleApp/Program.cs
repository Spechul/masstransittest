﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MassTransit;
using RabbitMQ.Client;
using Console = System.Console;

namespace ListenerConsoleApp
{
    public class Program
    {
        public static async Task Main()
        {
            var busControl = Bus.Factory.CreateUsingRabbitMq(cfg =>
            {
                cfg.Host("localhost", 9001, "/", h =>
                {
                    h.Username("guest");
                    h.Password("guest");
                });

                cfg.ReceiveEndpoint("priority-orders", x =>
                {
                    x.ConfigureConsumeTopology = false;

                    x.Consumer<OrderConsumer>();
                    x.Durable = true;

                    x.Bind("submitorder", s =>
                    {
                        s.RoutingKey = "PRIORITY";
                        s.ExchangeType = ExchangeType.Direct;
                    });
                });

                cfg.ReceiveEndpoint("regular-orders", x =>
                {
                    x.ConfigureConsumeTopology = false;

                    x.Consumer<OrderConsumer>();

                    x.Bind("submitorder", s =>
                    {
                        s.RoutingKey = "REGULAR";
                        s.ExchangeType = ExchangeType.Direct;
                    });
                });
            });

            var source = new CancellationTokenSource(TimeSpan.FromSeconds(10));

            await busControl.StartAsync(source.Token);
            try
            {
                Console.WriteLine("Press enter to exit");

                await Task.Run(() => Console.ReadLine());
            }
            finally
            {
                await busControl.StopAsync();
            }

        }
    }
}