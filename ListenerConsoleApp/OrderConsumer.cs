﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MassTransit;
using Messages;

public class OrderConsumer : IConsumer<SubmitOrder>
{
    private int i = 0;
    private object lockI;
    public async Task Consume(ConsumeContext<SubmitOrder> context)
    {
        Console.WriteLine($"{context.Message.CustomerType}");
    }
}