﻿using MassTransit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Messages;
using RabbitMQ.Client;

namespace ConsoleApp
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var busControl = Bus.Factory.CreateUsingRabbitMq(cfg =>
            {
                cfg.Host("localhost", 9001, "/", h =>
                {
                    h.Username("guest");
                    h.Password("guest");
                });
                cfg.Send<SubmitOrder>(x =>
                {
                        // use customerType for the routing key
                        x.UseRoutingKeyFormatter(context => context.Message.CustomerType);

                        // multiple conventions can be set, in this case also CorrelationId
                        x.UseCorrelationId(context => context.TransactionId);
                });
                //Keeping in mind that the default exchange config for your published type will be the full typename of your message
                //we explicitly specify which exchange the message will be published to. So it lines up with the exchange we are binding our
                //consumers too.
                cfg.Message<SubmitOrder>(x => x.SetEntityName("submitorder"));
                //Also if your publishing your message: because publishing a message will, by default, send it to a fanout queue. 
                //We specify that we are sending it to a direct queue instead. In order for the routingkeys to take effect.
                cfg.Publish<SubmitOrder>(x => x.ExchangeType = ExchangeType.Direct);
            });
            await busControl.StartAsync();
            //var client = busControl.CreateRequestClient<OrderRequest>();
            //var response = await client.GetResponse<OrderResult>(new OrderRequest() {Id = Guid.NewGuid()});
            var list = new List<Task>();
            for (int i = 0; i < 10000; i++)
            {
                if (i % 2 == 0)
                {
                    list.Add(busControl.Publish(new SubmitOrder()));
                }
                else
                {
                    list.Add(busControl.Publish(new SubmitOrder() { CustomerType = "asd" }));
                }
            }

            await Task.WhenAll(list);
            //Console.WriteLine(response.Message.Description);
            await busControl.StopAsync();
            Console.WriteLine("done");
            Console.ReadLine();
        }
    }
}
