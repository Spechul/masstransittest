﻿using System;

namespace Messages
{
    public class SubmitOrder
    {
        public string CustomerType { get; set; } = "REGULAR";
        public Guid TransactionId { get; set; }
    }

}